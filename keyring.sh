#!/bin/sh

# Script to scan Debian keyrings

set -eu

DESTDIR="${DEBHOME:-"$HOME/debian"}/keyring.debian.org"

if [ -z "${NONET:-}" ] ; then
echo "Syncing Debian Keyrings with rsync from keyring.debian.org"
rsync -av --block-size=8192 --delete --partial --progress \
	'keyring.debian.org::keyrings/' $DESTDIR
fi

KEYOPTIONS="--no-default-keyring --list-options=show-keyring"
for keyring in $DESTDIR/keyrings/*.gpg $DESTDIR/keyrings/*.pgp \
    /usr/share/keyrings/*.gpg ; do
	test -e $keyring || continue
	KEYOPTIONS="$KEYOPTIONS --keyring=$keyring"
done

gpg $KEYOPTIONS --fingerprint -k "$@" || :

grep -H --color=auto -i "$@" $DESTDIR/changelog || :
